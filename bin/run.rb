#!/usr/bin/env ruby

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)

require 'optimist'
require 'gitlab'

opts = Optimist.options do
  opt :hostname, 'Hostname for the GitLab instance', type: :string, default: 'gitlab.internal'
  opt :container_name, 'Name of the docker container to create', type: :string, default: 'gitlab'
  opt :gitlab_image, 'The docker image of GitLab to install', type: :string, default: 'gitlab/gitlab-ee:12.8.10-ee.0'
  opt :gitlab_token_scopes, 'The Scopes to be used for the access token generation', type: :string, default: ':read_user, :read_repository, :api, :sudo, :write_repository'
  opt :postgres_upgrade_version, 'The version of Postgres to be upgraded to', type: :string, default: '11'
  opt :skip_build, 'Skip the build stage and use an existing GitLab container', type: :flag, default: false
  opt :skip_token_creation, 'Will assume the access token is already created', type: :flag, default: false
  opt :skip_data_generation, 'Skip the data generation stage', type: :flag, default: false
  opt :skip_postgres_upgrade, 'Skip the Postgres upgrade process', type: :flag, default: false
end

gitlab = GitLab.new(container_name: opts[:container_name])

gitlab.remove_existing_containers unless opts[:skip_build]
gitlab.create_gitlab_container(hostname: opts[:hostname], gitlab_image: opts[:gitlab_image]) unless opts[:skip_build]

gitlab.wait_for_gitlab_ready

gitlab.create_root_access_token(scopes: opts[:gitlab_token_scopes]) unless opts[:skip_token_creation] || opts[:skip_data_generation]
gitlab.seed_data unless opts[:skip_data_generation]

gitlab.upgrade_postgres(version: opts[:postgres_upgrade_version]) unless opts[:skip_postgres_upgrade]