require 'json'

class GitLab
  def initialize(container_name:)
    @container_name = container_name
    @access_token = '1F2E3D4C5B6A99887766'
    @config_dir = File.expand_path('../assets', File.dirname(__FILE__))
  end

  def remove_existing_containers
    puts "Removing any existing containers with name #{@container_name}"
    `docker rm --force #{@container_name}`
  end

  def create_gitlab_container(hostname:, gitlab_image:)
    puts 'Creating GitLab container'
    `docker run --detach --hostname #{hostname} --publish 443:443 --publish 80:80 --publish 22:22 --name #{@container_name} -v #{@config_dir}/gitlab_config:/etc/gitlab:Z #{gitlab_image}`
  end

  def wait_for_gitlab_ready
    print 'Waiting for GitLab...'
    while container_starting?
      sleep(5)
      print '...'
    end
    sleep(60)
    puts "GitLab #{@gitlab_status}"
  end

  def container_starting?
    response = JSON.parse(`docker inspect #{@container_name}`)
    @gitlab_status = response[0]['State']['Health']['Status']
    @gitlab_status == 'starting'
  end

  def create_root_access_token(scopes:)
    puts 'Generating Access Token for root user'
    `docker exec #{@container_name} gitlab-rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: [#{scopes}], name: 'Automation token'); token.set_token('#{@access_token}'); token.save!"`
  end

  def seed_data
    puts 'Populating GitLab with test data'
    exec("docker run -it -e ACCESS_TOKEN=#{@access_token} -v #{@config_dir}/gpt_config:/config registry.gitlab.com/gitlab-org/quality/performance/gpt-data-generator --environment gpt-environment-config.json -u")
  end

  def upgrade_postgres(version:)
    puts "Upgrading Postgres to version #{version}"
    exec("docker exec #{@container_name} gitlab-ctl pg-upgrade -V #{version}")

    puts 'Deleting old postgres files'
    `docker exec #{@container_name} rm -f /var/opt/gitlab/postgresql-version.old`
    `docker exec #{@container_name} rm -rf /var/opt/gitlab/postgresql/data.*`
  end
end